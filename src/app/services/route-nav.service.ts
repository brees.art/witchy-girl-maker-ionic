import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class RouteNavService {

  constructor(
    private navCtr:NavController
  ) { }
  /**
   * GoToGame
   */
  public GoToGame() {
    return this.navCtr.navigateRoot('/game');
  }
  /**
   * GoToHome
   */
  public GoToHome() {
    return this.navCtr.navigateRoot('/');
  }
  /**
   * GoToSettings
   */
  public GoToSettings() {
    return this.navCtr.navigateForward('/settings');
  }

    /**
   * GoToGallery
   */
  public GoToGallery() {
    return this.navCtr.navigateForward('/gallery');
  }
}
