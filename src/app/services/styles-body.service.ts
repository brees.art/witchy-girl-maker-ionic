import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EyesResource } from './resources/eyes-resource';
import { EyebrowsResource } from './resources/eyebrows-resource';
import { BlousesResource } from './resources/blouses-resource';
import { FacesResource } from './resources/faces-resource';
import { FringeResource } from './resources/fringe-resource';
import { MouthResource } from './resources/mouth-resource';
import { BodyResources } from './resources/body-resources';

@Injectable({
  providedIn: 'root'
})

export class StylesBodyService {
  // Resources
  public NullImg = 'assets/null.png';
  public resource = {
    eyes: new EyesResource(),
    eyebrows: new EyebrowsResource(),
    blouses: new BlousesResource(),
    faces: new FacesResource(),
    fringes: new FringeResource(),
    mouth: new MouthResource(),
    body: new BodyResources()
  };
  
  // Elements
  public elements = {
    eyes: new BehaviorSubject(this.resource.eyes.Data.GetElement()),
    eyebrows: new BehaviorSubject(this.resource.eyebrows.Data.GetElement()),
    blouse: new BehaviorSubject(this.NullImg),
    faces: new BehaviorSubject(this.NullImg),
    fringes: new BehaviorSubject(this.NullImg),
    mouth: new BehaviorSubject(this.resource.mouth.Data.GetElement()),
    body: new BehaviorSubject(this.resource.body.Data.GetElement()),
  }

  public reset = {
    eyes: () => { this.elements.eyes.next(this.resource.eyes.Data.GetElement()) },
    eyebrows: () => { this.elements.eyebrows.next(this.resource.eyebrows.Data.GetElement()) },
    blouses: () => { this.elements.blouse.next(this.NullImg) },
    faces: () => { this.elements.faces.next(this.NullImg) },
    fringes: () => { this.elements.fringes.next(this.NullImg) },
    mouth: () => { this.elements.mouth.next(this.resource.mouth.Data.GetElement()) }
  };

  /**
   * SendDefault
   */
  public SendDefault() {
    Object.keys(this.reset).forEach(key => this.reset[key]());
  };
}
