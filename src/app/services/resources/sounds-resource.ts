import { Howl } from 'howler';

export class SoundsResource {
    public MainMusic: audio = new audio({
        file: 'Scary Tales(Loop).wav',
        id: 'mainmusic',
        loop: true
    });
    public ClickSound: audio = new audio({
        file: 'Bubble Click.wav',
        id: 'click',
        loop: false,
    });
}

class audio {
    private assestsDir = 'assets/wgm_resources/sounds/';
    private sound = null;
    private load = false;
    constructor(public dt: { file: string, id: string, loop: boolean }) {
    }

    /**
     * Load
     */
    public Load() {
        if (this.load == false) {
            this.sound = new Howl({ src: [this.assestsDir + this.dt.file], loop: this.dt.loop, });
            return
        }
        this.load = true
    }
    /**
     * Stop
     */
    public Stop() {
        if (this.sound) {
            this.sound.stop();
        }
    }

    /**
     * Play
     */
    public Play() {
        this.Load();
        if (this.sound) {
            this.sound.loop = this.dt.loop;
            this.sound.currentTime = 0;
            this.sound.play();
        }
    }
}


//interface audio {
//    file: string,
//    id: string,
//    loop: boolean
//}