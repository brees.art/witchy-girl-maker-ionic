import { RawMultListElementClass, colores_multi_select, ElementMult } from '../class-interface/multi-select';


function GenericGet(folder: string): ElementMult[] {
    return [
        { color: colores_multi_select.azulclaro, element: folder + '_azulclaro.png' },
        { color: colores_multi_select.naranja, element: folder + '_naranja.png' },
        { color: colores_multi_select.amarillo, element: folder + '_amarillo.png' },
        { color: colores_multi_select.marronclaro, element: folder + '_marronclaro.png' },
        { color: colores_multi_select.azuloscuro, element: folder + '_azuloscuro.png' },
        { color: colores_multi_select.blanco, element: folder + '_blanco.png' },
        { color: colores_multi_select.celeste, element: folder + '_celeste.png' },
        { color: colores_multi_select.lila, element: folder + '_lila.png' },
        { color: colores_multi_select.marron, element: folder + '_marron.png' },
        { color: colores_multi_select.negro, element: folder + '_negro.png' },
        { color: colores_multi_select.rojo, element: folder + '_rojo.png' },
        { color: colores_multi_select.rosa, element: folder + '_rosa.png' },
        { color: colores_multi_select.salmon, element: folder + '_salmon.png' },
        { color: colores_multi_select.turquesa, element: folder + '_turquesa.png' },
        { color: colores_multi_select.verde, element: folder + '_verde.png' },
        { color: colores_multi_select.verdeesmeralda, element: folder + '_verdesmeralda.png' },
        { color: colores_multi_select.violeta, element: folder + '_violeta.png' },
    ];
};

export class EyesResource {
    public Data: RawMultListElementClass = (new RawMultListElementClass([
        {
            elements: GenericGet('ojos01'),
            main: 10,
            subRoute: 'ojos01'
        },
        {
            elements: GenericGet('ojos02'),
            main: 10,
            subRoute: 'ojos02'
        },
        {
            main: 10,
            subRoute: '()ojos03',
            elements: GenericGet('ojos03')
        },
        {
            main: 10,
            subRoute: '()ojos04',
            elements: GenericGet('ojos04')
        },
        {
            main: 10,
            subRoute: 'ojos05',
            elements: GenericGet('ojos05')
        },
        {
            main: 0,
            subRoute: 'ojos06',
            elements: [
                { color: colores_multi_select.rosa, element: 'ojos06_rosa.png' },
            ]
        },
        {
            main: 0,
            subRoute: 'ojos07',
            elements: [
                { color: colores_multi_select.rosa, element: 'ojos07_rosa.png' },
            ]
        },
        {
            main: 0,
            subRoute: 'ojos08',
            elements: [
                { color: colores_multi_select.rosa, element: 'ojos08_rosa.png' },
            ]
        },
        {
            main: 0,
            subRoute: '()ojos09',
            elements: [
                { color: colores_multi_select.rosa, element: 'ojos09_rosa.png' },
            ]
        },
        {
            main: 0,
            subRoute: 'ojos10',
            elements: [
                { color: colores_multi_select.rosa, element: 'ojos10_rosa.png' },
            ]
        },
        {
            main: 0,
            subRoute: 'ojos11',
            elements: [
                { color: colores_multi_select.rosa, element: 'ojos11_rosa.png' },
            ]
        },
        {
            main: 0,
            subRoute: 'ojos12',
            elements: [
                { color: colores_multi_select.rosa, element: 'ojos12_rosa.png' },
            ]
        },
        {
            main: 0,
            subRoute: 'ojos13',
            elements: [
                { color: colores_multi_select.negro, element: 'ojos13.png' },
            ]
        }
        ,
        {
            main: 0,
            subRoute: 'ojos14',
            elements: [
                { color: colores_multi_select.rosa, element: 'ojos14_rosa.png' },
            ]
        },
        ,
        {
            main: 0,
            subRoute: 'ojos15',
            elements: [
                { color: colores_multi_select.negro, element: 'ojos15.png' },
            ]
        }
    ], '/assets/wgm_resources/eyes/')).SetMain({ element: 0, sub: 10 });

}
