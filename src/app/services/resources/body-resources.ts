import { RawListElementColorClass, colores_multi_select } from '../class-interface/multi-select';


export class BodyResources {
    public Data = new RawListElementColorClass({
        main:0,
        elements:[
            { color: colores_multi_select.c01, element: 'body01.png' },
            { color: colores_multi_select.c02, element: 'body02.png' },
            { color: colores_multi_select.c04, element: 'body04.png' },
            { color: colores_multi_select.c05, element: 'body05.png' },
            { color: colores_multi_select.c06, element: 'body06.png' },
            { color: colores_multi_select.c07, element: 'body07.png' },
            { color: colores_multi_select.c08, element: 'body08.png' },
            { color: colores_multi_select.c09, element: 'body09.png' },
            { color: colores_multi_select.c10, element: 'body10.png' },
            { color: colores_multi_select.c11, element: 'body11.png' },
            { color: colores_multi_select.c12, element: 'body12.png' },
            { color: colores_multi_select.c13, element: 'body13.png' },
            { color: colores_multi_select.c14, element: 'body14.png' },
            { color: colores_multi_select.c15, element: 'body15.png' },
            { color: colores_multi_select.c16, element: 'body16.png' },
            { color: colores_multi_select.c17, element: 'body17.png' },
            { color: colores_multi_select.c18, element: 'body18.png' },
        ],
        subRoute:''
    },'/assets/wgm_resources/body/');
}


