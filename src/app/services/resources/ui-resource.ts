export class UiResource {
    eyesIcon:icons = {
        disabled: "assets/wgm_resources/ui/Elementos/ojos.png",
        actived: "assets/wgm_resources/ui/Elementos/ojos2.png"
    }
    fringesIcon:icons = {
        disabled: "assets/wgm_resources/ui/Elementos/flequillo.png",
        actived: "assets/wgm_resources/ui/Elementos/flequillo2.png"
    }
    blousesIcon:icons = {
        disabled: "assets/wgm_resources/ui/Elementos/blusas.png",
        actived: "assets/wgm_resources/ui/Elementos/blusas2.png"
    }
    facesIcon:icons = {
        disabled: "assets/wgm_resources/ui/Elementos/cara.png",
        actived: "assets/wgm_resources/ui/Elementos/cara2.png"
    }
    bodyIcon:icons = {
        disabled: "assets/wgm_resources/ui/Elementos/cuerpo.png",
        actived: "assets/wgm_resources/ui/Elementos/cuerpo2.png"
    }
    eyebrowsIcon:icons = {
        disabled: "assets/wgm_resources/ui/Elementos/cejas.png",
        actived: "assets/wgm_resources/ui/Elementos/cejas2.png"
    }

    mouthIcon:icons = {
        disabled: "assets/wgm_resources/ui/Elementos/boca.png",
        actived: "assets/wgm_resources/ui/Elementos/boca2.png"
    }
    hairstyleIcon:icons = {
        disabled: "assets/wgm_resources/ui/Elementos/cabello atras.png",
        actived: "assets/wgm_resources/ui/Elementos/cabello atras2.png"
    }
}

interface icons {
    actived:string,
    disabled: string
}