import { RawMultListElementClass, colores_multi_select, ElementMult, RawListElements } from 'src/app/services/class-interface/multi-select';

function GenericGet(folder: string): ElementMult[] {
    return [
        { element: folder + '_amarillo.png', color: colores_multi_select.amarillo },
        { element: folder + '_amarillopollito.png', color: colores_multi_select.amarillopollito },
        { element: folder + '_azulclaro.png', color: colores_multi_select.azulclaro },
        { element: folder + '_azuloscuro.png', color: colores_multi_select.azuloscuro },
        { element: folder + '_blanco.png', color: colores_multi_select.blanco },
        { element: folder + '_celeste.png', color: colores_multi_select.celeste },
        { element: folder + '_lila.png', color: colores_multi_select.lila },
        { element: folder + '_marronclaro.png', color: colores_multi_select.marronclaro },
        { element: folder + '_marronoscuro.png', color: colores_multi_select.marron },
        { element: folder + '_naranja.png', color: colores_multi_select.naranja },
        { element: folder + '_negro.png', color: colores_multi_select.negro },
        { element: folder + '_rojo.png', color: colores_multi_select.rojo },
        { element: folder + '_rosa.png', color: colores_multi_select.rosa },
        { element: folder + '_salmon.png', color: colores_multi_select.salmon },
        { element: folder + '_turquesa.png', color: colores_multi_select.turquesa },
        { element: folder + '_verde.png', color: colores_multi_select.verde },
        { element: folder + '_verdesmeralda.png', color: colores_multi_select.verdeesmeralda },
        { element: folder + '_violeta.png', color: colores_multi_select.violeta },
    ];
};

function GenericAll(main: number, pre: string, more = { block: false, folder: '' }): RawListElements {
    return {
        main: main,
        subRoute: more.folder ? more.folder : pre,
        elements: GenericGet(pre),
        block: more.block
    };
}

export class FringeResource {
    public Data: RawMultListElementClass = new RawMultListElementClass(
        [
            GenericAll(17, 'flequillo01'),
            GenericAll(17, 'flequillo02'),
            GenericAll(17, 'flequillo03'),
            GenericAll(17, 'flequillo04'),
            GenericAll(17, 'flequillo05'),
            GenericAll(17, 'flequillo06'),
            GenericAll(17, 'flequillo07', { block: true, folder: '()flequillo07' }),
            GenericAll(17, 'flequillo08'),
            GenericAll(17, 'flequillo09'),
            GenericAll(17, 'flequillo10'),
            GenericAll(17, 'flequillo11'),
            GenericAll(17, 'flequillo12'),
            GenericAll(17, 'flequillo13'),
            GenericAll(17, 'flequillo14'),
            GenericAll(17, 'flequillo15'),
        ],
        '/assets/wgm_resources/fringes/',
    )
}
