export const colores_multi_select = {
    azulclaro: 'azulclaro.svg',
    naranja: 'naranja.svg',
    amarillo: 'amarillo.svg',
    marronclaro: 'marronclaro.svg',
    azuloscuro: 'azuloscuro.svg',
    blanco: 'blanco.svg',
    celeste: 'celeste.svg',
    lila: 'lila.svg',
    marron: 'marron.svg',
    negro: 'negro.svg',
    rojo: 'rojo.svg',
    rosa: 'rosa.svg',
    salmon: 'salmon.svg',
    turquesa: 'turquesa.svg',
    verde: 'verde.svg',
    verdeesmeralda: 'verdeesmeralda.svg',
    violeta: 'violeta.svg',
    bicolor1: 'bicolor1.svg',
    amarillopollito:'amarillopollito.svg',
    c01: 'c01.svg',
    c02: 'c02.svg',
    c04: 'c04.svg',
    c05: 'c05.svg',
    c06: 'c06.svg',
    c07: 'c07.svg',
    c08: 'c08.svg',
    c09: 'c09.svg',
    c10: 'c10.svg',
    c11: 'c11.svg',
    c12: 'c12.svg',
    c13: 'c13.svg',
    c14: 'c14.svg',
    c15: 'c15.svg',
    c16: 'c16.svg',
    c17: 'c17.svg',
    c18: 'c18.svg',
}


export class RawMultListElementClass {
    constructor(
        public elements: RawListElements[],
        private dirAssets: string,
    ) {

    }
    public GetElement(): string {
        const sub_route = this.elements[this.selectElement.element].subRoute;
        const name = this.elements[this.selectElement.element].elements[this.selectElement.sub].element;
        return this.dirAssets + sub_route + '/' + name;
    }


    public GetListParse(): ParseListElements[] {
        const multiparse = this.GetRawListElementColorClassNull();
        let rtdt: ParseListElements[] = [];
        this.elements.forEach(x =>
            rtdt.push(multiparse.SetElements(x).GetListParse())
        );
        return rtdt;
    }

    private GetRawListElementColorClassNull(): RawListElementColorClass {
        return new RawListElementColorClass({ elements: [], main: 0, subRoute: '' }, this.dirAssets);
    }

    public selectElement = {
        sub: 0,
        element: 0
    };
    /**
     * SetMain
     */
    public SetMain(dt: {sub:number,element:number}):RawMultListElementClass {
        this.selectElement = dt;
        return this;
    }
}


export class RawListElementColorClass {
    constructor(
        public elements: RawListElements,
        private dirAssets: string,
    ) {

    }

    public GetElement(): string {
        const sub_route = this.elements.subRoute;
        const name = this.elements.elements[0].element;
        return this.dirAssets + this.GetSub(sub_route) + name;
    }

    private GetSub(sub: string) {
        return (sub == '') ? '' : sub + '/'
    }


    /**
     * SetElements
     */
    public SetElements(dt: RawListElements): RawListElementColorClass {
        this.elements = dt;
        return this
    }



    public GetListParse(): ParseListElements {
        let rtdt: ParseListElements;
        rtdt = {
            main: this.dirAssets + this.GetSub(this.elements.subRoute) + this.elements.elements[this.elements.main].element,
            elementes: this.elements.elements.map(fr => {
                return { block:fr.block, color: fr.color, element: this.dirAssets + this.GetSub(this.elements.subRoute) + fr.element };
            }),
        };
        return rtdt;
    }
}

export class RawListElementClass {
    constructor(
        public elements: string[],
        private dirAssets: string,
    ) {

    }
    public GetElement(): string {
        return this.dirAssets + this.elements[0];
    }

    /**
     * GetListParse
     */
    public GetListParse() {
        return this.elements.map(x => this.dirAssets + x);
    }
}




export interface RawListElements {
    main: number;
    elements: ElementMult[];
    subRoute: string;
    block?:boolean;
}



export interface ParseListElements {
    main: string;
    elementes: ElementMult[];
}


export interface ElementMult {
    element: string;
    color: string;
    block?:boolean;
}

