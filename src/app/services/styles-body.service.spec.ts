import { TestBed } from '@angular/core/testing';

import { StylesBodyService } from './styles-body.service';

describe('StylesBodyService', () => {
  let service: StylesBodyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StylesBodyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
