import { TestBed } from '@angular/core/testing';

import { RouteNavService } from './route-nav.service';

describe('RouteNavService', () => {
  let service: RouteNavService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RouteNavService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
