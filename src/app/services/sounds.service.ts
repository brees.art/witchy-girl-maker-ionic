import { Injectable } from '@angular/core';
import { SoundsResource } from './resources/sounds-resource';


@Injectable({
  providedIn: 'root'
})
export class SoundsService {
  public soundResource: SoundsResource = new SoundsResource();
  public playedPrimaryMusic = false;
  constructor(
  ) {
  }


 

  /**
   * PlayClickSound
   */
  public PlayClickSound() {
    this.soundResource.ClickSound.Play()
  }


  /**
   * PlayPrimaryMusic
   */
  public PlayPrimaryMusic(loop: boolean) {
    this.playedPrimaryMusic = true;
    this.soundResource.MainMusic.Play()
  }

  /**
   * StopPrimaryMusic
   */
  public StopPrimaryMusic() {
    this.playedPrimaryMusic = false;
    this.soundResource.MainMusic.Stop()
  }
}
