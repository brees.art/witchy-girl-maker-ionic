import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { GamePageRoutingModule } from './game-routing.module';
import { GamePage } from './game.page';
// Maker
import { MainSelectComponent } from './componets/selects/main-select/main-select.component';
import { BlousesSelectComponent } from './componets/selects/blouses-select/blouses-select.component';
import { FringeSelectComponent } from './componets/selects/fringe-select/fringe-select.component';
import { EyesSelectComponent } from './componets/selects/eyes-select/eyes-select.component';
import { FacesSelectComponent } from './componets/selects/faces-select/faces-select.component';
import { EyebrowsSelectComponent } from './componets/selects/eyebrows-select/eyebrows-select.component';
import { MulticolorSelectComponent } from './componets/selects/multicolor-select/multicolor-select.component'
import { MouthSelectComponent } from './componets/selects/mouth-select/mouth-select.component';
import { BodySelectComponent } from './componets/selects/body-select/body-select.component';
import { ColorSelectComponent } from './componets/selects/color-select/color-select.component';
import { GameAllBodyComponent } from './componets/game-all-body/game-all-body.component';
import { BodyModuleModule } from 'src/app/module/body-module/body-module/body-module.module';
import { AndroidFullScreen } from '@ionic-native/android-full-screen/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GamePageRoutingModule,
    BodyModuleModule
  ],
  declarations: [
    GamePage,
    // Selects
    MainSelectComponent,
    BlousesSelectComponent,
    FringeSelectComponent,
    EyesSelectComponent,
    FacesSelectComponent,
    EyebrowsSelectComponent,
    MulticolorSelectComponent,
    MouthSelectComponent,
    BodySelectComponent,
    ColorSelectComponent,
    // Maker
    GameAllBodyComponent,
  ],
  entryComponents: [
  ],
  providers:[
    AndroidFullScreen
  ]
})
export class GamePageModule { }
