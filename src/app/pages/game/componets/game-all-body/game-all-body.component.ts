import { Component, OnInit } from '@angular/core';
import { StylesBodyService } from 'src/app/services/styles-body.service';

@Component({
  selector: 'app-game-all-body',
  templateUrl: './game-all-body.component.html',
  styleUrls: ['./game-all-body.component.scss'],
})
export class GameAllBodyComponent implements OnInit {
  public body = {
    body: this.styleBody.NullImg,
    eyes: this.styleBody.NullImg,
    blouses: this.styleBody.NullImg,
    fringe: this.styleBody.NullImg,
    mouth: this.styleBody.NullImg,
    faces: this.styleBody.NullImg,
    eyebrows: null,
  };

  constructor(
    private styleBody: StylesBodyService
  ) { }

  ngOnInit() {
    this.styleBody.elements.body.subscribe(v =>{
      this.body.body = v;
    });
    this.styleBody.elements.eyes.subscribe(v =>{
      this.body.eyes = v;
    });
    this.styleBody.elements.blouse.subscribe(x =>{
      this.body.blouses = x;
    });
    this.styleBody.elements.fringes.subscribe(x =>{
      this.body.fringe = x;
    });
    this.styleBody.elements.mouth.subscribe(v =>{
      this.body.mouth = v;
    });
    this.styleBody.elements.faces.subscribe(v =>{
      this.body.faces = v;
    });
    this.styleBody.elements.eyebrows.subscribe(v =>{
      this.body.eyebrows = v;
    })
  }
}

