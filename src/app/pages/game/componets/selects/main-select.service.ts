import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { StylesBodyService } from 'src/app/services/styles-body.service';

export type selectionPage = 'blouses' | 'eyebrows' | 'eyes' | 'faces' | 'fringes' | 'dress' | 'body' | 'mouth' | 'hairstyle';

@Injectable({
  providedIn: 'root'
})
export class MainSelectService {
  public ReloadSelectPage: Subject<boolean> = new (Subject);
  public ColorsSelectElement: Subject<boolean> = new (Subject);

  public selectPage: selectionPage = 'eyes';
  public colorView = {
    blouses: false,
    eyebrows: false,
    eyes: true,
    faces: true,
    fringes: true,
    dress: false,
    body: false,
    mouth: false,
    hairstyle: true,
  };
  constructor(
    private styleBody: StylesBodyService
  ) { }

  public GoToPage(page: selectionPage) {
    if (page == this.selectPage) {
      this.ReloadSelectPage.next(true);
    }
    this.selectPage = page;
  }

  /**
   * RemoveUltimate
   */
  public RemoveUltimate() {
    this.styleBody.reset[this.selectPage] ? this.styleBody.reset[this.selectPage]() : console.log('No se encontro' + this.selectPage);
  }
}


