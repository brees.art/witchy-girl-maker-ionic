import { Component, OnInit, OnDestroy } from '@angular/core';
import { StylesBodyService } from 'src/app/services/styles-body.service';
import { ParseListElements } from 'src/app/services/class-interface/multi-select';

@Component({
  selector: 'app-fringe-select',
  templateUrl: './fringe-select.component.html',
  styleUrls: ['./fringe-select.component.scss'],
})
export class FringeSelectComponent implements OnInit {
  public list: ParseListElements[] = [];
  constructor(
    public styleBody: StylesBodyService,
  ) { }
  ngOnInit() {
    this.list = this.styleBody.resource.fringes.Data.GetListParse();
  }
  public set = (x:string) =>{
    this.styleBody.elements.fringes.next(x ? x : this.styleBody.NullImg);
  }
}

