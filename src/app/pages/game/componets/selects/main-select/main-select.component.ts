import { Component, OnInit, ViewChild } from '@angular/core';
// import { BlousesSelectComponent } from '../blouses-select/blouses-select.component';
// import { FringeSelectComponent } from '../fringe-select/fringe-select.component';
// import { EyesSelectComponent } from '../eyes-select/eyes-select.component';
// import { FacesSelectComponent } from '../faces-select/faces-select.component';
import { SoundsService } from 'src/app/services/sounds.service';
import { MainSelectService, selectionPage } from '../main-select.service';
import { UiResource } from 'src/app/services/resources/ui-resource';


@Component({
  selector: 'app-main-select',
  templateUrl: './main-select.component.html',
  styleUrls: ['./main-select.component.scss'],
})
export class MainSelectComponent implements OnInit {
  public icons = new(UiResource)
  constructor(
    private soundServ:SoundsService,
    public mainSelectSer:MainSelectService,
  ) { }
  
  ngOnInit() {
  }
  public LaunchComponent(component:selectionPage) {
    this.soundServ.PlayClickSound();
    this.icons.blousesIcon.actived
    this.mainSelectSer.GoToPage(component);
  }
  
}

