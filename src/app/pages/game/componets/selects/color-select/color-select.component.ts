import { Component, OnInit, Input } from '@angular/core';
import { ParseListElements } from 'src/app/services/class-interface/multi-select';

@Component({
  selector: 'app-color-select',
  templateUrl: './color-select.component.html',
  styleUrls: ['./color-select.component.scss'],
})
export class ColorSelectComponent {
  @Input('list') select: ParseListElements;
  @Input('set') set: Function;
  constructor() { }
}
