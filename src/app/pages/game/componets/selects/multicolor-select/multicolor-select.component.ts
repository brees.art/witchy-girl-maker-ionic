import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ParseListElements } from 'src/app/services/class-interface/multi-select';
import { MainSelectService } from '../main-select.service';

@Component({
  selector: 'app-multicolor-select',
  templateUrl: './multicolor-select.component.html',
  styleUrls: ['./multicolor-select.component.scss'],
})
export class MulticolorSelectComponent implements OnInit, OnDestroy {
  @Input('set') set: Function;
  @Input('list') list: ParseListElements[];
  @Input('stylemain') stylemain: string;
  public select: ParseListElements = null;
  public colorSelect = false;
  constructor(
    private selectSer: MainSelectService,
  ) { }
  ngOnInit() { }

  /**
 * setIndex
 */
  public setIndex(x: ParseListElements) {
    if (x == this.select) {
      this.colorSelect =true;
      return
    }
    this.select = x;
    this.set(x.main);
  }

  private reload = this.selectSer.ReloadSelectPage.subscribe(x => {
    this.select = null;
    this.colorSelect = false;
  });

  private color = this.selectSer.ColorsSelectElement.subscribe(x => {
    if (this.select) {
      this.colorSelect = !this.colorSelect;
    }
  });

  ngOnDestroy() {
    this.reload.unsubscribe();
    this.color.unsubscribe();
  }

}
