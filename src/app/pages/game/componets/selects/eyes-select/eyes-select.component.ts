import { Component, OnInit } from '@angular/core';
import { StylesBodyService } from 'src/app/services/styles-body.service';
import { ParseListElements } from 'src/app/services/class-interface/multi-select';

@Component({
  selector: 'app-eyes-select',
  templateUrl: './eyes-select.component.html',
  styleUrls: ['./eyes-select.component.scss'],
})
export class EyesSelectComponent implements OnInit {
  public list: ParseListElements[] = [];
  constructor(
    public styleBody: StylesBodyService,
  ) { }
  ngOnInit() {
    this.list = this.styleBody.resource.eyes.Data.GetListParse();
  }
  public set = (x:string) =>{
    this.styleBody.elements.eyes.next(x ? x : this.styleBody.NullImg);
  }
}
