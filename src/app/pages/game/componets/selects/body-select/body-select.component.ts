import { Component, OnInit } from '@angular/core';
import { ParseListElements } from 'src/app/services/class-interface/multi-select';
import { StylesBodyService } from 'src/app/services/styles-body.service';

@Component({
  selector: 'app-body-select',
  templateUrl: './body-select.component.html',
  styleUrls: ['./body-select.component.scss'],
})
export class BodySelectComponent implements OnInit {
  public list: ParseListElements;
  constructor(
    public styleBody: StylesBodyService,
  ) { }
  ngOnInit() {
    this.list = this.styleBody.resource.body.Data.GetListParse();
  }

  public set = (x:string) =>{
    this.styleBody.elements.body.next(x ? x : this.styleBody.resource.body.Data.GetElement());
  }
}
