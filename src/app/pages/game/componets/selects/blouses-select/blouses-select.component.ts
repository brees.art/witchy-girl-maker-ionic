import { Component, OnInit } from '@angular/core';
import { StylesBodyService } from 'src/app/services/styles-body.service';

@Component({
  selector: 'app-blouses-select',
  templateUrl: './blouses-select.component.html',
  styleUrls: ['./blouses-select.component.scss'],
})
export class BlousesSelectComponent implements OnInit {
  public list = [];
  constructor(
    private styleBody: StylesBodyService,
  ) { }

  ngOnInit() {
    this.list = this.styleBody.resource.blouses.Data.GetListParse()
  }

  /**
   * set
   */
  public set(x: string) {
    this.styleBody.elements.blouse.next(x ? x : 'assets/null.png');
  }
}
