import { Component, OnInit } from '@angular/core';
import { StylesBodyService } from 'src/app/services/styles-body.service';

@Component({
  selector: 'app-faces-select',
  templateUrl: './faces-select.component.html',
  styleUrls: ['./faces-select.component.scss'],
})
export class FacesSelectComponent implements OnInit {
  public list = [];
  constructor(
    private styleBody: StylesBodyService,
  ) { }

  ngOnInit() {
    this.list = this.styleBody.resource.faces.Data.GetListParse()

  }

  /**
   * set
   */
  public set(x: string) {
    this.styleBody.elements.faces.next(x ? x : 'assets/null.png');
  }
}
