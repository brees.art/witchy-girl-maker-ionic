import { Component, OnInit } from '@angular/core';
import { StylesBodyService } from 'src/app/services/styles-body.service';

@Component({
  selector: 'app-eyebrows-select',
  templateUrl: './eyebrows-select.component.html',
  styleUrls: ['./eyebrows-select.component.scss'],
})
export class EyebrowsSelectComponent implements OnInit {
  public list = [];
  constructor(
    private styleBody: StylesBodyService,
  ) { }

  ngOnInit() {
    this.list = this.styleBody.resource.eyebrows.Data.GetListParse()
  }

  /**
   * set
   */
  public set(x: string) {
    this.styleBody.elements.eyebrows.next(x ? x : 'assets/null.png');
  }
}
