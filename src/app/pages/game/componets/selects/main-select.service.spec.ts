import { TestBed } from '@angular/core/testing';

import { MainSelectService } from './main-select.service';

describe('MainSelectService', () => {
  let service: MainSelectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MainSelectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
