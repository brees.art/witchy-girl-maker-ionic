import { Component, OnInit } from '@angular/core';
import { StylesBodyService } from 'src/app/services/styles-body.service';

@Component({
  selector: 'app-mouth-select',
  templateUrl: './mouth-select.component.html',
  styleUrls: ['./mouth-select.component.scss'],
})
export class MouthSelectComponent implements OnInit {
  public list = [];
  constructor(
    private styleBody: StylesBodyService,
  ) { }

  ngOnInit() {
    this.list = this.styleBody.resource.mouth.Data.GetListParse()
  }

  /**
   * set
   */
  public set(x: string) {
    this.styleBody.elements.mouth.next(x ? x : this.styleBody.resource.mouth.Data.GetElement());
  }
}
