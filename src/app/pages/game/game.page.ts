import { Component, OnInit, OnDestroy, } from '@angular/core';
import { SoundsService } from 'src/app/services/sounds.service';
import { StylesBodyService } from 'src/app/services/styles-body.service';
import { RouteNavService } from 'src/app/services/route-nav.service';
import { ModalController, Platform, ToastController } from '@ionic/angular';
import { MainSelectService } from './componets/selects/main-select.service';
import { Screenshot } from '@ionic-native/screenshot/ngx';
import { AndroidFullScreen } from '@ionic-native/android-full-screen/ngx';


@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
})
export class GamePage implements OnInit, OnDestroy {
  constructor(
    public soundServ: SoundsService,
    private styleBody: StylesBodyService,
    private nav: RouteNavService,
    public mainSelect: MainSelectService,
    private modalController: ModalController,
    private screenshot: Screenshot,
    private androidFullScreen: AndroidFullScreen,
    private platform: Platform,
    private toastController: ToastController,
  ) {
  }

  ngOnInit() {
    this.PrepareGame();
  }

  /**
   * SaveImage
   */
  public SaveImage() {
    let time = Math.floor(Date.now() / 1000);
    this.screenshot.save('jpg', 80, `witchyGirlMaker/${time}.jpg`).then((ok) => {
      this.toast('Guardado correctamente', 1000,'top');
    }, err => {
      this.toast('Error al guardar', 1000,'top');
    });
  }
  private toast(text: string, time = 2000, position: 'top' | 'bottom' | 'middle' = 'bottom') {
    this.toastController.create({
      message: text,
      duration: time,
      position: position,
    }).then(x => x.present());
  }

  private PrepareGame() {
    this.soundServ.soundResource.MainMusic.Load();
    setTimeout(() => {
      this.modalController.getTop().then(v => v ? v.dismiss() : true);
      this.soundServ.PlayPrimaryMusic(true);
    }, 1000);
    if (this.platform.is('android')) {
      this.androidFullScreen.isImmersiveModeSupported()
    }
  }


  ngOnDestroy() {
    this.soundServ.StopPrimaryMusic();
  }
  private ClickFab() {
    this.soundServ.PlayClickSound();
  }

  /**
   * GoToHome
   */
  public GoToHome() {
    this.soundServ.PlayClickSound();
    this.nav.GoToHome();
  }
  /**
   * RemoveUltimate
   */
  public RemoveUltimate() {
    this.ClickFab();
    this.mainSelect.RemoveUltimate()
  }

  /**
   * ColorsSelectElement
   */
  public ColorsSelectElement() {
    this.ClickFab();
    this.mainSelect.ColorsSelectElement.next(true);
  }


  /**
   * SendDefault
   */
  public SendDefault() {
    this.ClickFab();
    this.styleBody.SendDefault();
  }
}
