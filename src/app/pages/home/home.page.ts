import { Component, OnInit } from '@angular/core';
import { SoundsService } from 'src/app/services/sounds.service';
import { RouteNavService } from 'src/app/services/route-nav.service';
import { ModalController, ToastController } from '@ionic/angular';
import { SplashComponent } from './componets/splash/splash.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  private clicktitle = 0;
  private debug = false;
  private textRadom = [
    'Hola Personita buscas algo interesante?',
    'Parece que te perdiste, vamos presiona "iniciar"',
    'Ummm, eso son muchos click',
    'Si precionas "iniciar" encontraras algo interesante',
    'Todo bien? Te regalo un abrazo UwU',
    'Enserio?, Woo es genial NwN',
    'De cuanto es la probabilidad que encontraras esto?',
    'De cuanto es la probabilidad de ver alguno de estos mensajes?',
    'Mucho texto U.U',
    '#w',
    '304.4 MBI',
    'No es la unica sorpresa >.<',
    '@breesciarpa',
    'Guarda tus mejores creaciones y compartelas con la comunidad.',
    'No se me ocurre nada -.-',
  ];
  constructor(
    private soundServ: SoundsService,
    private nav: RouteNavService,
    private modalController: ModalController,
    private toastController: ToastController
  ) { }

  ngOnInit() {
    this.soundServ.soundResource.ClickSound.Load();
  }
  /**
   * click
   */
  public click() {
    if (this.debug == true) {
      this.soundServ.PlayClickSound();
      this.randomMessage();
      return;
    }
    (this.clicktitle++) == 3 ? this.debug = true : '';
    if (this.debug) {
      this.toast('Debug is runing')
    }
  }

  private randomMessage() {
    if (Math.random() >= 0.7) {
      const v = this.textRadom[Math.floor(Math.random() * this.textRadom.length)];
      this.toast(v, 3000);
    }
    if (Math.random() >= 0.97) {
      this.toast('@AndrusCodex', 3000,'top');
    }
  }

  private toast(text: string, time = 2000, position: 'top' | 'bottom' | 'middle' = 'bottom') {
    this.toastController.create({
      message: text,
      duration: time,
      position: position,
    }).then(x => x.present());
  }
  /**
   * gotToGame
   */
  public gotToGame() {
    this.soundServ.PlayClickSound();
    this.Load({ texto: 'PREPARANDO..' }).finally(() => this.nav.GoToGame())
  }
  /**
   * gotToSettings
   */
  public gotToSettings() {
    this.soundServ.PlayClickSound();
    this.Load({ texto: 'SETTINGS..' }).finally(() => this.nav.GoToSettings())
  }
  /**
   * gotToGallery
   */
  public gotToGallery() {
    this.soundServ.PlayClickSound();
    this.Load({ texto: 'GALLERY..' }).finally(() => this.nav.GoToGallery())
  }
  private Load(props: { texto: string }): Promise<any> {
    if (this.debug) {
      return new Promise((x, err) => x(true));
    }
    return this.modalController.create({ component: SplashComponent, componentProps: props }).then(v => v.present());
  }
}
