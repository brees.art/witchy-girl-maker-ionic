import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GalleryPageRoutingModule } from './gallery-routing.module';

import { GalleryPage } from './gallery.page';
import { BodyModuleModule } from 'src/app/module/body-module/body-module/body-module.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GalleryPageRoutingModule,
    // Module
    BodyModuleModule
  ],
  declarations: [GalleryPage]
})
export class GalleryPageModule {}
