import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FringeComponent } from './fringe.component';

describe('FringeComponent', () => {
  let component: FringeComponent;
  let fixture: ComponentFixture<FringeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FringeComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FringeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
