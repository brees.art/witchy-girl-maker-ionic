import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-fringe',
  templateUrl: './fringe.component.html',
  styleUrls: ['./fringe.component.scss'],
})
export class FringeComponent {
  @Input() element_src:string;
  constructor(
  ) { }
}