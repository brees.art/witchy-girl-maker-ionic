import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EyesComponent } from './eyes.component';

describe('EyesComponent', () => {
  let component: EyesComponent;
  let fixture: ComponentFixture<EyesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EyesComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EyesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
