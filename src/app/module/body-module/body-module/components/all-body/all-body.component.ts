import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-all-body',
  templateUrl: './all-body.component.html',
  styleUrls: ['./all-body.component.scss'],
})
export class AllBodyComponent {
  @Input('data') data;
  constructor() { }
}
