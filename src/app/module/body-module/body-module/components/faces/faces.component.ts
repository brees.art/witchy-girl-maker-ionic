import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-faces',
  templateUrl: './faces.component.html',
  styleUrls: ['./faces.component.scss'],
})
export class FacesComponent {
  @Input() element_src:string;
  constructor(
  ) { }
}