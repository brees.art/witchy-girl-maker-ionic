import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacesComponent } from './faces.component';

describe('FacesComponent', () => {
  let component: FacesComponent;
  let fixture: ComponentFixture<FacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacesComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
