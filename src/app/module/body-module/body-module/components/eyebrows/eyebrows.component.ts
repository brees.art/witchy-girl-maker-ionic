import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-eyebrows',
  templateUrl: './eyebrows.component.html',
  styleUrls: ['./eyebrows.component.scss'],
})
export class EyebrowsComponent {
  @Input() element_src:string;
  constructor(
  ) { }
}
