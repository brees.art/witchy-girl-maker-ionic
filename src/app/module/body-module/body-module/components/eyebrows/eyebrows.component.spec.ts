import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EyebrowsComponent } from './eyebrows.component';

describe('EyebrowsComponent', () => {
  let component: EyebrowsComponent;
  let fixture: ComponentFixture<EyebrowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EyebrowsComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EyebrowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
