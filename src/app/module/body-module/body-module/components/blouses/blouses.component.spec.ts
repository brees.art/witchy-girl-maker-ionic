import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlousesComponent } from './blouses.component';

describe('BlousesComponent', () => {
  let component: BlousesComponent;
  let fixture: ComponentFixture<BlousesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlousesComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlousesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
