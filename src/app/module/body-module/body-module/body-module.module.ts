import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BodyComponent } from './components/body/body.component';
import { EyesComponent } from './components/eyes/eyes.component';
import { BlousesComponent } from './components/blouses/blouses.component';
import { FringeComponent } from './components/fringe/fringe.component';
import { MouthComponent } from './components/mouth/mouth.component';
import { EyebrowsComponent } from './components/eyebrows/eyebrows.component';
import { FacesComponent } from './components/faces/faces.component';
import { AllBodyComponent } from './components/all-body/all-body.component';



@NgModule({
  declarations: [
    // Maker
    BodyComponent,
    EyesComponent,
    BlousesComponent,
    FringeComponent,
    MouthComponent,
    EyebrowsComponent,
    FacesComponent,
    AllBodyComponent
  ],
  exports:[
    BodyComponent,
    EyesComponent,
    BlousesComponent,
    FringeComponent,
    MouthComponent,
    EyebrowsComponent,
    FacesComponent,
    AllBodyComponent
  ],
  imports: [
    CommonModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BodyModuleModule { }
